import org.joda.money.Money;

/**
 * Created by sfontinelle on 2/6/17.
 */
public class Product {

    private int id;
    private Money price;

    public Product(int id, String price) {
        this.id = id;
        this.price = Money.parse(price);
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = Money.parse("USD " +price);
    }
}
