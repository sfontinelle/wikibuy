import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.money.Money;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by sfontinelle on 2/6/17.
 */
public class Bundle {

    private int bundleId;
    private Collection<Product> products;
    private Money total;

    public Bundle(int id, Collection<Product> products, Money total) {
        this.bundleId = id;
        this.products = products;
        this.total = total;
    }

    public Bundle() {
    }

    public List<Integer> getProductIds(){
        List<Integer> ids = new ArrayList();
        for(Product product: products){
            ids.add(product.getId());
        }
        return ids;
    }




    public int getBundleId() {
        return bundleId;
    }

    public void setBundleId(int bundleId) {
        this.bundleId = bundleId;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Money getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = Money.parse("USD " +total);
    }


}
