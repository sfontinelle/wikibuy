import java.math.BigDecimal;
import java.util.Collection;

/**
 * Created by sfontinelle on 2/6/17.
 */
public class Output {

    private Collection<Integer> bundleIds;
    private BigDecimal orderTotal;


    public Output(Collection<Integer> bundleIds, BigDecimal orderTotal) {
        this.bundleIds = bundleIds;
        this.orderTotal = orderTotal;
    }

    public Output() {
    }

    public Collection<Integer> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(Collection<Integer> bundleIds) {
        this.bundleIds = bundleIds;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }
}
