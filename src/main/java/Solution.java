import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.joda.money.Money;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by sfontinelle on 2/6/17.
 */
public class Solution {

    public static void main(String[] args) throws IOException {

        String problemDirectory = args[0];
        String productsFile = problemDirectory + "/products.json";
        String bundlesFile = problemDirectory + "/bundles.json";

        List<Product> products = loadProducts(productsFile);
        List<Bundle> bundles = loadBundles(bundlesFile);
        List<List<Integer>> matchingBundles = findMatchingBundles(products, bundles);
        Collection<Bundle> lowestPriceBundles = findLowestPriceBundles(matchingBundles, bundles);
        output(lowestPriceBundles);

    }

    private static List<Product> loadProducts(String productFile) throws IOException {

        File productJson = new File(productFile);

        //http://stackoverflow.com/a/35920152
        ObjectMapper objectMapper = new ObjectMapper();
        Product[] pojos = objectMapper.readValue(productJson, Product[].class);
        List<Product> products = Arrays.asList(pojos);
        return products;
    }

    private static List<Bundle> loadBundles(String bundlesFile) throws IOException {


        File productJson = new File(bundlesFile);

        //http://stackoverflow.com/a/35920152
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(
            PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        Bundle[] pojos = objectMapper.readValue(productJson, Bundle[].class);
        List<Bundle> bundles = Arrays.asList(pojos);
        return bundles;
    }

    private static List<List<Integer>> findMatchingBundles(Collection<Product> products,
                                                           Collection<Bundle> bundles){

        List<Integer> productIds = new ArrayList<Integer>();
        for(Product product: products){
            productIds.add(product.getId());
        }

        List<Bundle> bundleList = new ArrayList(bundles);
        Stack<Integer> potentialBundles = new Stack<Integer>();
        List<List<Integer>> completeBundles = new ArrayList<List<Integer>>();

        recursiveBundleFinder(bundleList,productIds,potentialBundles,completeBundles);

        return completeBundles;

    }

    private static void recursiveBundleFinder(List<Bundle> bundleList,
                                              List<Integer> productIds,
                                              Stack<Integer> potentialBundles,
                                              List<List<Integer>> completeBundles){

        for(int i = 0; i< bundleList.size() && productIds.size() > 0;i++) {

            Bundle currentBundle = bundleList.get(i);
            Stack<Integer> removedProductIds = new Stack();
            boolean isPotentialBundle = true;

            for (Product bundleProduct : currentBundle.getProducts()) {

                if (productIds.contains(bundleProduct.getId())) {
                   Integer productid = (Integer)bundleProduct.getId();
                    productIds.remove( productid );
                    removedProductIds.push(productid);
                } else {
                    productIds.addAll(removedProductIds);
                    removedProductIds.clear();
                    isPotentialBundle = false;
                    break;
                }
            }

            if (isPotentialBundle) {
                potentialBundles.push(currentBundle.getBundleId());
                if (productIds.isEmpty()) {
                    //found a complete potential list of bundles
                    List<Integer> potentialBundlesCopy = new ArrayList<Integer>(potentialBundles);
                    completeBundles.add(potentialBundlesCopy);
                }
                //recurse
                recursiveBundleFinder(bundleList.subList(i + 1, bundleList.size()), productIds, potentialBundles, completeBundles);
                potentialBundles.pop();
                productIds.addAll(currentBundle.getProductIds());
            }
        }

    }

    private static Collection<Bundle> findLowestPriceBundles(List<List<Integer>> bundleIdLists,List<Bundle> bundles){


        Money lowestTotal = Money.parse("USD " +Integer.MAX_VALUE);
        List<Integer> lowestTotalBundleIdList = new ArrayList<Integer>();

        for(List<Integer> bundleIds: bundleIdLists){
            Money total = Money.parse("USD 0");
            for(Integer bundleId: bundleIds){
                Bundle bundle = bundles.get(bundleId);
                total = Money.total(total,bundle.getTotal());
            }
            if(total.isLessThan(lowestTotal)){
                lowestTotal = total;
                lowestTotalBundleIdList = bundleIds;
            }
        }

        Collection<Bundle> lowestTotalBundleCollection = new ArrayList<Bundle>();
        for(Integer bundleId: lowestTotalBundleIdList){
            lowestTotalBundleCollection.add(bundles.get(bundleId));
        }

        return lowestTotalBundleCollection;

    }

    private static void output(Collection<Bundle> outputBundles) throws JsonProcessingException {

        Money orderTotalMoney = Money.parse("USD 0");
        ArrayList<Integer> bundleIds = new ArrayList<Integer>();
        for(Bundle bundle:outputBundles){
            bundleIds.add(bundle.getBundleId());
            orderTotalMoney = Money.total(orderTotalMoney,bundle.getTotal());
        }

        final BigDecimal orderTotal = orderTotalMoney.getAmount();
        Output output = new Output(bundleIds, orderTotal);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(
                PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        String outputJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(output);

        System.out.println(outputJson);
    }

}


/*
Product Bundling

Background:
One of the problems Impossible Ventures deals with is how to group (or bundle) products together to get the best
possible price for our customers.  For instance, if a customer orders products X, Y, and Z, we may find the best
possible price by ordering products X and Z from retailer A and product Y from retailer B.

Goal:
Given a set of products (think: items in your shopping cart) and a set of possible bundles, find the cheapest set of
bundles that covers all products within an order.

Requirements:
Write a solution that takes in the products from products.json and considers all bundles in bundles.json and returns an
array of bundles where the set contains exactly all products once and has the lowest possible price to complete the
order.

Files:
[dataset]/products.json
[dataset]/bundles.json

Output:
{
	"bundle_ids": [], // ex [1,2,3]
	"order_total": 0 // ex 123.45
}

We've provided 4 sample data sets for you to test your code against: [small, medium, original, large].  Each data set
is in its own directory, e.g. "small/" and contains a products.json file, a bundles.json file, and a solution.json file.
We've also provided a simple test harness so you can check your output against the solution.  You can run the test
harness as follows:

$ [your program and its arguments] | python harness.py [dataset directory]

e.g., if your code is called bundle.py and takes as a parameter the data set directory:

$ python bundle.py small/ | python harness.py small/
 */